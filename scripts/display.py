import torch
import torchvision.transforms.functional as tt
import argparse
from PIL import Image
import matplotlib.pyplot as plt
import scripts.models as M

def extract_arguments():
    """defines the arguments the script requires and returns the argparse
    object"""

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'image_path', type=str,
        help='path to the the image'
    )
    parser.add_argument(
        'input_size', type=int,
        help='resolution of the degraded image'
    )
    parser.add_argument(
        'output_size', type=int,
        help='target resolution'
    )
    parser.add_argument(
        'batch_size', type=int,
        help='batch_size for training'
    )
    return vars(parser.parse_args())

def extract_image(path:str, in_shape:int, out_shape:int):
    """extract the image from the disk, and format for input to the network"""

    #extract image
    image = Image.open(path)
    #perform image transformations
    x = tt.to_tensor(tt.resize(image, (in_shape, in_shape))).unsqueeze(0)
    y = tt.to_tensor(tt.resize(image, (out_shape, out_shape))).unsqueeze(0)
    return (x, y)

def restore_image(image:torch.tensor):
    """the output of the network is not expected to be in a standardized form
    therefore all we must do is convert it back to an image, although it should
    still be normalized"""

    image = image.squeeze()
    image = torch.clamp(image, 0, 1)
    return tt.to_pil_image(image)

def display_result(result:dict):
    """display all the images from the demonstrator"""

    for key in result.keys():
        value = result[key]
        plt.imshow(restore_image(value))
        plt.title(key)
        plt.axis('off')
        plt.show()

def main():

    #parse command line arguments
    arguments = extract_arguments()
    #extract the input
    #construct the demonstrator
    demonstrator = M.Demonstrator(
        None,
        arguments['input_size'],
        arguments['output_size'],
    )
    #extract the recovered images
    (x, y) = extract_image(
        arguments['image_path'],
        arguments['input_size'],
        arguments['output_size'],
    )
    result = demonstrator.recover(x, y)
    display_result(result)

main()
