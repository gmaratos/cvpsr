import os
import random
from PIL import Image
import torch
import torch.utils.data as tu
import torch.nn.functional as F
import torchvision.transforms as tt

random.seed(0)

class ImageSet(tu.Dataset):
    def __init__(self, data_path, input_size, output_size, split):
        """data_path: path to a folder than contains images"""

        #create image path mapping
        image_names = os.listdir(data_path)
        image_paths = []
        for name in image_names:
            image_paths.append(os.path.join(data_path, name))
        #select the correct split
        factor = int(len(image_paths)*0.1)
        random.shuffle(image_paths)
        set_mapping = {
            'train': image_paths[:factor*7],
            'val': image_paths[factor*7:factor*8],
            'test': image_paths[factor*8:]
        }
        try:
            image_paths = set_mapping[split]
        except KeyError:
            print('Error: {} is not a split'.format(split))
            image_paths = None
        #define image transformations if we train, then augment
        if split == 'train':
            initial_transforms = tt.Compose([
                tt.Resize((output_size, output_size)),
                tt.RandomHorizontalFlip(),
                tt.RandomVerticalFlip(),
                tt.RandomRotation(30),
                tt.ToTensor(),
            ])
        else:
            initial_transforms = tt.Compose([
                tt.Resize((output_size, output_size)),
                tt.ToTensor(),
            ])
        #save data members
        self.input_size = input_size
        self.image_paths = image_paths
        self.initial_transforms = initial_transforms

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, index):
        path = self.image_paths[index]
        image = Image.open(path)
        (x, y) = self.generate_image_pair(image)
        return (x, y)

    def generate_image_pair(self, image):

        #can I normalize before interpolate?
        #first apply random transformations to the image
        transformed_image = self.initial_transforms(image)
        transformed_image = transformed_image.unsqueeze(0)
        #now downsample the image to get input
        downsampled_image = F.interpolate(
            transformed_image,
            size=(self.input_size, self.input_size),
            mode='bilinear'
        )
        #next normalize the input
        downsampled_image = torch.squeeze(downsampled_image)
        #squeeze the output so it has same dimensions as input but no normalize
        transformed_image = torch.squeeze(transformed_image)
        return downsampled_image, transformed_image
