import os, sys
from scripts.ssim import SSIM
from scripts.dataset import ImageSet

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as td
import torchvision.transforms as tt

import numpy as np

from tqdm import tqdm

#define image standardizer

def find_device():
    if torch.cuda.is_available():
        device = torch.device('cuda:1')
    else:
        device = torch.device('cpu')
    return device

class BaseNet(nn.Module):
    """base network that all NN models inherit, it contains code for running the
    training loop and validation which are common to all networks here"""

    def __init__(self):
        super(BaseNet, self).__init__()
        self.ssim = SSIM() #can pass parameters later
        self.device = find_device()

    def forward(self, x):
        pass

    def fit(self, train_data, val_data, batch_size, epochs=1, lr=1e-2):
        """train a neural network model with the given
        parameters"""

        #first place model on device
        self.to(self.device)

        #create datasets
        train_loader = td.DataLoader(
            train_data,
            batch_size=batch_size,
            num_workers=5,
            shuffle=True,
        )
        val_loader = td.DataLoader(
            val_data,
            batch_size=batch_size,
            num_workers=5,
            shuffle=True,
        )

        #create optimizer
        optimizer = torch.optim.Adam(
            self.parameters(),
        )

        #print some information about the run
        print('Training on Device < {} >'.format(self.device))
        print('Total Epochs: {}'.format(epochs))
        print('Lr: {}'.format(lr))
        print('Batch Size: {}'.format(batch_size))
        print('')

        #run training loop
        for i in range(epochs):
            #run gradient descent using training data
            print('Epoch {}'.format(i+1))
            self.train(True)
            train_loss = []
            t = tqdm(train_loader)
            for batch in t:
                #run forward pass
                (x, y) = batch
                x = x.to(self.device)
                y = y.to(self.device)
                prediction = self.forward(x)
                loss = F.mse_loss(prediction, y)
                #backpropogration
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                #record metrics
                train_loss.append(loss.item())
                #update tqdm
                t.set_postfix(loss='{:.3e}'.format(loss.item()))
                t.set_description(desc='Train')
            avg_loss = sum(train_loss)/len(train_loss)
            #estimate performance metrics
            self.validation(val_loader) #validation prints its results
            print('\tAvg Train Loss: {:.3e}\n'.format(avg_loss))

        #save weights for later
        self.save_weights()

    def validation(self, data):
        val_psnr, val_ssim, val_loss = [], [], []
        t = tqdm(data)
        tv = TotalVariation(self.device)
        with torch.no_grad():
            self.train(False)
            for batch in t:
                (x, y) = batch
                x = x.to(self.device)
                y = y.to(self.device)
                prediction = self.forward(x)
                loss = F.mse_loss(prediction, y)
                val_psnr.append(self.calculate_psnr(loss))
                val_ssim.append(self.calculate_ssim(prediction, y))
                val_loss.append(loss.item())
                t.set_description(desc='Validation')
            #calculate results
            avg_psnr = sum(val_psnr)/len(val_psnr)
            avg_ssim = sum(val_ssim)/len(val_ssim)
            avg_loss = sum(val_loss)/len(val_loss)
            #report results
            print('\tAvg PSNR: {:.3f}'.format(avg_psnr))
            print('\tAvg SSIM: {:.3e}'.format(avg_ssim))
            print('\tAvg Val Loss: {:.3e}'.format(avg_loss))

    def calculate_psnr(self, mse):
        return 10*torch.log10(1/mse).item()#max value is 1

    def calculate_ssim(self, prediction, y):
        return self.ssim(prediction, y)

    #utility functions for serializing model

    def verify_state(self):
        #the state is to have a name and the directory weights exists
        if not hasattr(self, 'name'):
            return False
        if not os.path.isdir('weights'):
            os.makedirs('weights')
        return True

    def save_weights(self):

        #put model on cpu
        self.to(torch.device('cpu'))
        #verify state
        if not self.verify_state():
            print('Warning: No weights saved because model type unknown')
            return
        #now save the weights
        path = os.path.join(
            'weights',
            '{}.pt'.format(self.name)
        )
        torch.save(self.state_dict(), path)

    def load_weights(self):

        #verify state
        if not self.verify_state():
            print('Warning: No weights loaded because model type unknown')
            return
        #try to load weights
        path = os.path.join(
            'weights',
            '{}.pt'.format(self.name)
        )
        try:
            self.load_state_dict(torch.load(path))
        except FileNotFoundError:
            print('Warning: No weights loaded because there are none in "weights" dir')

class Interpolator(BaseNet):
    """a simple upsample, overwrites much of the training
    procedure"""

    def __init__(self, output_size):
        super(Interpolator, self).__init__()
        #input comes normalized for networks so we must denormalize
        #this is the only model that does this so we require this hack
        self.upsample = nn.Upsample(size=(output_size, output_size), mode='bicubic')

    def forward(self, x):
        #now interpolate
        return self.upsample(x)

    def fit(self, train_data, val_data, batch_size, epochs=1, lr=1e-2):
        """overwrite base net here because no gradient descent is needed,
        the optional parameters are for maintaining compatibility and are
        ignored, also since there are no weights there is no saving"""

        #create datasets
        train_loader = td.DataLoader(
            train_data,
            batch_size=batch_size,
            num_workers=10,
            shuffle=True,
        )
        val_loader = td.DataLoader(
            val_data,
            batch_size=batch_size,
            num_workers=5,
            shuffle=True,
        )

        print('Interpolator does not train, calculating Training Loss')
        train_loss = []
        t = tqdm(train_loader)
        for batch in t:
            #run forward pass
            (x, y) = batch
            x = x.to(self.device)
            y = y.to(self.device)
            prediction = self.forward(x)
            loss = F.mse_loss(prediction, y)
            train_loss.append(loss.item())
            t.set_description(desc='Train')
        avg_loss = sum(train_loss)/len(train_loss)

        print('Now Calculating Validation')
        #estimate performance metrics
        self.validation(val_loader) #validation prints its results
        print('\tAvg Train Loss: {:.3f}\n'.format(avg_loss))


class InterpSharp(Interpolator):
    def __init__(self, output_size):
        super(InterpSharp, self).__init__(output_size)
        filt = [[-0.00296902, -0.01330621, -0.02193823, -0.01330621, -0.00296902],
            [-0.01330621, -0.0596343,  -0.09832033, -0.0596343,  -0.01330621],
            [-0.02193823, -0.09832033,  1.83789718, -0.09832033, -0.02193823],
            [-0.01330621, -0.0596343,  -0.09832033, -0.0596343,  -0.01330621],
            [-0.00296902, -0.01330621, -0.02193823, -0.01330621, -0.00296902]
        ]
        filt = np.array(filt)[np.newaxis, np.newaxis, ...].repeat(3, axis=0)
        self.kernel = torch.nn.Parameter(torch.tensor(filt, dtype=torch.float))

    def forward(self, x):
        x = self.upsample(x)
        return torch.clamp(F.conv2d(x, self.kernel, padding=2, groups=3), 0, 1)

class PreUpsample(BaseNet):
    """First Upsample, then have network learn fuzzy -> sharp"""

    def __init__(self, output_size):
        super(PreUpsample, self).__init__()
        self.name = 'PreUpsample'
        self.output_size = output_size
        filters = 20
        #conv layers
        self.layer1 = nn.Conv2d(3, filters, 3, padding=1, bias=False)
        self.layer2 = nn.Conv2d(filters, filters*2, 3, padding=1, bias=False)
        self.layer3 = nn.Conv2d(filters*2, filters*3, 3, padding=1, bias=False)
        self.layer4 = nn.Conv2d(filters*3, 3, 3, padding=1, bias=False)
        #batch norm
        self.bn1 = nn.BatchNorm2d(filters)
        self.bn2 = nn.BatchNorm2d(2*filters)
        self.bn3 = nn.BatchNorm2d(3*filters)
        #misc
        self.upsample = nn.Upsample(size=(output_size, output_size))

    def forward(self, x):
        x = self.upsample(x)
        x = F.relu(self.bn1(self.layer1(x)))
        x = F.relu(self.bn2(self.layer2(x)))
        x = F.relu(self.bn3(self.layer3(x)))
        x = self.layer4(x)
        return x

class ResPreUpsample(PreUpsample):
    """A preupsample network that learns the residule instead of a complex
    transformation from image to image"""

    def __init__(self, output_size):
        super(ResPreUpsample, self).__init__(output_size)
        self.name = 'ResPreUpsample'

    def forward(self, x):
        x = self.upsample(x)
        base = x #save the upsampled image
        x = F.relu(self.bn1(self.layer1(x)))
        x = F.relu(self.bn2(self.layer2(x)))
        x = F.relu(self.bn3(self.layer3(x)))
        x = self.layer4(x)
        return x + base #only difference from normal

class PostUpsample(BaseNet):
    """run computations in the lower dimension, then upsample at the end"""
    def __init__(self, input_size, output_size):
        super(PostUpsample, self).__init__()
        filters = 20
        self.name = 'PostUpsample'
        self.up1 = SPUpsampleBlock(3, input_size, output_size, filters=filters)
        self.upsample = nn.Upsample(size=(output_size, output_size), mode='bicubic')
        self.cls = nn.Conv2d(filters, 3, 3, padding=1)

    def forward(self, x):
        base = self.upsample(x)
        x = self.up1(x)
        x = self.cls(x) + base
        return x

class DeepResidulePostSample(BaseNet):
    def __init__(self, input_size, output_size):
        super(DeepResidulePostSample, self).__init__()
        filters = 25
        factor = int(output_size / input_size)
        self.name = 'DRPSample'
        self.factor = factor

        self.upsample = nn.Upsample(size=(output_size, output_size))

        self.embed = nn.Conv2d(3, filters, kernel_size=5, padding=2, bias=False)
        self.bn1 = nn.BatchNorm2d(filters)

        self.backbone = nn.Sequential(
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
            ResiduleBlock(filters),
        )

        self.up1 = nn.Conv2d(filters, filters*factor**2, 3, padding=1, bias=False)
        self.bn6 = nn.BatchNorm2d(filters)

        self.cls = nn.Conv2d(filters, 3, kernel_size=3, padding=1, bias=False)

    def forward(self, x):
        base = self.upsample(x)
        x = F.relu(self.bn1(self.embed(x)))
        x = self.backbone(x)
        x = F.pixel_shuffle(self.up1(x), self.factor)
        x = F.relu(self.bn6(x))
        return self.cls(x) + base

class ResiduleBlock(nn.Module):
    def __init__(self, filters):
        super(ResiduleBlock, self).__init__()

        self.conv2 = nn.Conv2d(filters, filters, kernel_size=3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(filters)

        self.conv3 = nn.Conv2d(filters, filters, kernel_size=3, padding=1, bias=False)
        self.bn3 = nn.BatchNorm2d(filters)

        self.conv4 = nn.Conv2d(filters, filters, kernel_size=3, padding=1, bias=False)
        self.bn4 = nn.BatchNorm2d(filters)

        self.conv5 = nn.Conv2d(filters, filters, kernel_size=3, padding=1, bias=False)
        self.bn5 = nn.BatchNorm2d(filters)

    def forward(self, x):
        x = x + F.relu(self.bn2(self.conv2(x)))
        x = x + F.relu(self.bn3(self.conv3(x)))
        x = x + F.relu(self.bn4(self.conv4(x)))
        x = x + F.relu(self.bn5(self.conv5(x)))
        return x


class ProgUpsample(BaseNet):
    """do progressive upsampling instead of all at once, but keep residule
    learning, the progressive upsampling splits the 4x into two upsamples"""

    def __init__(self, input_size, output_size):
        super(ProgUpsample, self).__init__()
        filters = 20
        self.name = 'ProgUpsample'
        self.upsample = nn.Upsample(size=(output_size, output_size))
        self.up1 = SPUpsampleBlock(3, input_size, input_size*2, filters=filters)
        self.up2 = SPUpsampleBlock(filters, input_size*2, output_size, filters=filters)
        self.cls = nn.Conv2d(filters, 3, 3, padding=1)

    def forward(self, x):
        base = self.upsample(x)
        x = self.up1(x)
        x = self.up2(x)
        x = self.cls(x) + base
        return x

class IterativeSample(BaseNet):

    def __init__(self, input_size, output_size):
        super(IterativeSample, self).__init__()
        filters = 30
        self.name = 'IterativeSample'
        self.embed1 = nn.Conv2d(3, filters, kernel_size=5, padding=2)
        self.embed2 = nn.Conv2d(filters, filters, kernel_size=1)
        self.upsample = nn.Upsample(size=(output_size, output_size))
        self.up1 = SPUpsampleBlock(filters, input_size, output_size, filters=filters)
        self.d1 = DownSampleBlock(filters, filters)
        self.up2 = SPUpsampleBlock(2*filters, input_size, output_size, filters=2*filters)
        self.d2 = DownSampleBlock(3*filters, 3*filters)
        self.up3 = SPUpsampleBlock(6*filters, input_size, output_size, filters=6*filters)
        self.cls = nn.Conv2d(6*filters, 3, kernel_size=3, padding=1)

    def forward(self, x):
        base = self.upsample(x)

        l1 = F.relu(self.embed2(self.embed1(x)))
        h1 = self.up1(l1)
        l2 = self.d1(h1)

        l2 = torch.cat([l1, l2], dim=1)
        h2 = self.up2(l2)

        h2 = torch.cat([h1, h2], dim=1)
        l3 = self.d2(h2)

        l3 = torch.cat([l1, l2, l3], dim=1)
        h3 = self.up3(l3)

        return self.cls(h3) + base


class SPUpsampleBlock(nn.Module):
    """This is an block that uses a subpixel layer to upsample. Dimensions
    from the channels are reshaped to add width and height. up1 takes the
    target dimension and multiplies it by 4 to get a doubling effect."""

    def __init__(self, input_channels, input_dim, output_dim, filters):
        super(SPUpsampleBlock, self).__init__()
        factor = int(output_dim / input_dim)
        self.c1 = nn.Conv2d(input_channels, filters, 3, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(filters)
        self.c2 = nn.Conv2d(filters, filters, 3, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(filters)
        self.up1 = nn.Conv2d(filters, filters*factor**2, 3, padding=1, bias=False)
        self.bn3 = nn.BatchNorm2d(filters)
        self.factor = factor

    def forward(self, x):
        #first do some convolutions
        x = F.relu(self.bn1(self.c1(x)))
        x = F.relu(self.bn2(self.c2(x)))
        #now upsample
        x = F.pixel_shuffle(self.up1(x), self.factor)
        x = F.relu(self.bn3(x))
        return x

class DownSampleBlock(nn.Module):
    """downsamples the image, by a factor of two"""

    def __init__(self, input_channels, filters):
        super(DownSampleBlock, self).__init__()
        self.c1 = nn.Conv2d(input_channels, filters, 8, stride=4, padding=2)
        self.bn1 = nn.BatchNorm2d(filters)

    def forward(self, x):
        return F.relu(self.bn1(self.c1(x)))

class Demonstrator:
    """demonstrate the performance of all the trained models on the test set,
    and also support single image inference for demonstrative purposes."""

    def __init__(self, data_path:str, input_size:int, output_size:int):

        #build dataset object
        if data_path == None:
            test_data = None
        else:
            test_data = ImageSet(
                data_path,
                input_size,
                output_size,
                'test'
            )
        #create data members all models
        self.device = find_device()
        self.pre_up = ResPreUpsample(output_size).to(self.device)
        self.post_up = PostUpsample(input_size, output_size).to(self.device)
        self.prog_up = ProgUpsample(input_size, output_size).to(self.device)
        self.baseline = Interpolator(output_size)
        self.baseline2 = InterpSharp(output_size).to(self.device)
        self.test_data = test_data

        #load weights
        self.pre_up.load_weights()
        self.post_up.load_weights()
        self.prog_up.load_weights()

    def compute_metrics(self, output:torch.tensor, target:torch.tensor):
        mse = F.mse_loss(output, target)
        #using the baselines BaseModel interface
        psnr = self.baseline.calculate_psnr(mse)
        ssim = self.baseline.calculate_ssim(output, target)
        return {
            'mse': mse,
            'psnr': psnr,
            'ssim': ssim,
        }

    def run_all_inference(self, input_size:int, output_size:int, batch_size:int):

        #build test dataset
        test_loader = td.DataLoader(
            self.test_data,
            batch_size=batch_size,
            num_workers=5,
            shuffle=False,
        )
        #run inference
        with torch.no_grad():
            print('Baseline: ')
            self.baseline.validation(test_loader)
            print('Baseline Sharp:')
            self.baseline2.validation(test_loader)
            print('')
            print('PreUpsample: ')
            self.pre_up.validation(test_loader)
            print('')
            print('PostUpsample: ')
            self.post_up.validation(test_loader)
            print('')
            print('ProgUpsample: ')
            self.prog_up.validation(test_loader)
            print('')

    def recover(self, x:torch.tensor, y:torch.tensor):
        """run the restoration process with all models"""

        y_base = self.baseline(x)
        y_sharp = self.baseline2(x)
        y_pre = self.pre_up(x)
        y_post = self.post_up(x)
        y_prog = self.prog_up(x)
        return {
            'degraded': x,
            'truth': y,
            'Baseline': y_base,
            'Baseline Sharp': y_sharp,
            'PreUpsample': y_pre,
            'PostUpsample': y_post,
            'ProgUpsample': y_prog,
        }
