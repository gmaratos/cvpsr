from scripts.dataset import ImageSet
import scripts.models as models

import argparse

def build_arguments():
    """defines the arguments the script requires and returns the argparse
    object"""

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'data_path', type=str,
        help='path to the folder containing the images'
    )
    parser.add_argument(
        'input_size', type=int,
        help='resolution of the degraded image'
    )
    parser.add_argument(
        'output_size', type=int,
        help='target resolution'
    )
    parser.add_argument(
        'batch_size', type=int,
        help='batch_size for training'
    )
    return parser

#parse cmd args
parser = build_arguments()
arguments = vars(parser.parse_args())
input_size = arguments['input_size']
output_size = arguments['output_size']
batch_size = arguments['batch_size']

#build dataset and model
train_data = ImageSet(
    arguments['data_path'],
    input_size,
    output_size,
    'train'
)
val_data = ImageSet(
    arguments['data_path'],
    input_size,
    output_size,
    'val'
)
bilinear_model = models.Interpolator(output_size)
#pre_upsample_model = models.PreUpsample(output_size)
#prog_upsample_model = models.ProgressiveUpsample()
#prog_upsample_model.load_weights()
#res_PUnet = models.ResPreUpsample(output_size)
#post_up_model = models.PostUpsample(input_size, output_size)
prog_up_model = models.ProgUpsample(input_size, output_size)
prog_up_model.load_weights()
#iup_model = models.IterativeSample(input_size, output_size)
#iup_model.load_weights()

prog_up_model.fit(
    train_data,
    val_data,
    batch_size,
    epochs=10
)
