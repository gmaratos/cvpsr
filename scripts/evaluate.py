"""Script that demonstrates super resolution results for a single image"""

import sys
import argparse
import torch
from PIL import Image
import scripts.models as M
import torchvision.transforms as ttn
import torchvision.transforms.functional as tt
import torch.nn.functional as F
import torch.utils.data as td
from scripts.dataset import ImageSet

def extract_arguments():
    """defines the arguments the script requires and returns the argparse
    object"""

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'data_path', type=str,
        help='path to the the data'
    )
    parser.add_argument(
        'input_size', type=int,
        help='resolution of the degraded image'
    )
    parser.add_argument(
        'output_size', type=int,
        help='target resolution'
    )
    parser.add_argument(
        'batch_size', type=int,
        help='batch_size for training'
    )
    return vars(parser.parse_args())

def extract_image(path:str, in_shape:int, out_shape:int):
    """extract the image from the disk, and format for input to the network"""

    #extract image
    image = Image.open(path)
    #perform image transformations
    x = tt.to_tensor(tt.resize(image, (in_shape, in_shape)))
    y = tt.to_tensor(tt.resize(image, (out_shape, out_shape)))
    return (x, y)

def restore_image(image:torch.tensor):
    """the output of the network is not expected to be in a standardized form
    therefore all we must do is convert it back to an image, although it should
    still be normalized"""

    return tt.to_pil_image(image)

def main():

    #parse command line arguments
    arguments = extract_arguments()
    #extract the input
    #construct the demonstrator
    demonstrator = M.Demonstrator(
        arguments['data_path'],
        arguments['input_size'],
        arguments['output_size'],
    )
    #run inference
    demonstrator.run_all_inference(
        arguments['input_size'],
        arguments['output_size'],
        arguments['batch_size'],
    )

main()
